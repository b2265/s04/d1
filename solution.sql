-- Find all artists that has letter d in its name.

SELECT * FROM artists WHERE name LIKE "%d%";

-- Find all song that has a lenght of less than 4 minutes.

SELECT * FROM songs WHERE music_length <= 400;

-- Join the 'albums' and 'songs' tables (only show the album name, song name and song length)

SELECT albums.album_title, songs.song_name, songs.music_length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)


SELECT artists.name, albums.album_title, albums.date_released, artists_id FROM artists
	LEFT JOIN albums ON artists.id = albums.artists_id
	WHERE album_title LIKE "%a%";

-- Sort the albums in Z--a order. (Show only the first 4 records.)

SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;


-- Join the 'albums' and 'songs' tables and find all songs greater than 3 minutes 30 seconds. (Sort albums from Z-A)

SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id
	WHERE music_length > 330
	ORDER BY album_title DESC;

