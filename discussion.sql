-- MySQL Advances Queries and Joins

-- [SECTION] Add new records
-- Add 5 artists, atleaste 2 albums each, 1-2 songs per album.
-- Add 5 artists

INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justine Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

SELECT * FROM artists;

-- atleast 2 albums each
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Fearless", "2008-11-11", 3);
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Red", "2012-10-22", 3);

INSERT INTO albums (album_title, date_released, artists_id) VALUES ("A Star Is Born", " 2018-10-05 ", 4);
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Born This Way", "2011-05-23", 4);

INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Purpose", "2015-11-13", 5);
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Believe", "2012-06-15", 5);

INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Dangerous Woman", "2016-05-20", 6);
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("EDM House", "2019-02-08", 6);

INSERT INTO albums (album_title, date_released, artists_id) VALUES ("24k Magic", "2016-11-18 ", 7);
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Earth to Mars", "2011-02-07 ", 7);

-- insert 2 songs each

-- Taylor Swift
INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("Fearless", 402, "Pop Rock", 3);

INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("Love Story", 355, "Country Pop", 3);

INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("State of Grace", 455, "Rock, Alternative Rock, Arena Rock", 4);

INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("Love Story", 341, "Country", 4);

-- Lady Gaga
INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("lack Eyes", 304, "Rock and Roll", 5);

INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("Shallow", 336, "Country, Rock, Folk Rock", 5);

INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("Born This Way", 420, "Electropop", 6);

-- JB

INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("Sorry", 320, "Dancehall-poptropical Housemoombahton", 7);

INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("Boyfriend", 252, "Pop", 8);

-- Ariana Grande
INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("Into You", 405, "EDM House", 9);

INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("Thank U, Next", 327, "Pop, R&B", 10);

-- Bruno Mars
INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("24k Magic", 346, "Funk, Disco, R&B", 11);

INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("TLost", 327, "Pop", 12);

SELECT * FROM albums;
SELECT * FROM Songs;

-- [SECTION] Advanced Selects

-- Exclude records
SELECT * FROM songs WHERE id != 11;

-- LIMIT: Show only specific number of records.
SELECT * FROM songs LIMIT 5;

-- Greater than or equal / Less than or equal (Comparison Operators)
SELECT * FROM songs WHERE id <= 11;
SELECT * FROM songs WHERE id >=11;

-- Get specific records using "OR"
SELECT * FROM songs WHERE id = 1 OR id = 5 OR id = 9;

-- Get specific records using "IN" 
-- shrt hand method for multiple OR conditions.
SELECT *  FROM songs WHERE id IN (1, 5, 9);

SELECT * FROM songs WHERE music_genre IN ("Pop", "KPOP");

-- Combining conditions
SELECT * FROM songs WHERE album_id = 4 AND id < 8;

-- Find Partial matches.
-- LIKE is used in a WHERE clause tto serach for a specified patten in a column.
-- There are two wildcards used in conjuction with LIKE.
	-- (%) - represents zero, one or multiple characters.
	-- (_) - Repreents a single character. 2012-_-_;

SELECT * FROM songs WHERE song_name LIKE "%e"; -- select keywords from the end.

SELECT * FROM songs WHERE song_name LIKE "b%"; -- select keywords from the start.

SELECT * FROM songs WHERE song_name LIKE "%a%"; -- select keywords from the middle

SELECT * FROM songs WHERE music_genre LIKE "%pop%";

SELECT * FROM albums WHERE date_released LIKE "____-__-__";

SELECT * FROM albums WHERE date_released LIKE "201_-0_-0_";

-- Sorting records (alphanumeric order)
-- Syntax: SELECT * FROM table_name ORDER BY column_name ASC/DESC;
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

SELECT * FROM songs ORDER BY song_name ASC LIMIT 5;

-- Getting disctinct records.
-- Eliminates duplicate rows and display a unique list of values.
SELECT DISTINCT music_genre FROM songs;

-- [SECTION] Table joins
-- This is used to retrieve data from multiple tables
-- This is perfromed whenever two ro more tables are listed in a SQL statement.
--- Visual Joins: https://joins.spathon.com/

-- Syntax oof Inner Join (Join)
/*
	Two Tables:

	SELECT * (or column_name) FROM table1_name 
		JOIN table2_name ON table1_column_id_pk = table2_column_id_fk;

	Multiple Tables:

	SELECT * (or column_name) FROM table1_name 
		JOIN table2_name ON table1_column_id_pk = table2_column_id_fk;
		JOIN table3_name ON table2_column_id_pk = table3_column_id_fk;

*/

-- Combine artists and albums table.
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artists_id;

-- Combine more than two tables
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artists_id
	JOIN songs ON albums.id = songs.album_id;


-- Specify columns to be included in hte Join table.
SELECT artists.name, albums.album_title, songs.song_name FROM artists
	JOIN albums ON artists.id = albums.artists_id
	JOIN songs ON albums.id = songs.album_id;


-- Insert another artists

INSERT INTO artists (name) VALUES ("Ed Sheeran");

-- Show artist without records on the right side of the joined table.
SELECT artists.name, albums.album_title FROM artists
	LEFT JOIN albums ON artists.id = albums.artists_id;

SELECT artists.name, albums.album_title FROM artists
	RIGHT JOIN albums ON artists.id = albums.artists_id;

-- FULL OUTER JOIN
	-- UNION: used to combine the result of 2 or more SELECT statements;
SELECT artists.name, albums.album_title FROM artists
	LEFT JOIN albums ON artists.id = albums.artists_id
	UNION
SELECT artists.name, albums.album_title FROM artists
	RIGHT JOIN albums ON artists.id = albums.artists_id;
